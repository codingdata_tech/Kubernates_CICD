import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './Store'
import Navbar from './Components/Navbar/Navbar'
import Footer from './Components/Footer/Footer'
import PetList from './Components/ListingPage/PetList/petList'
import About from './Components/About/About'
import './App.css'
import vetList from './Components/ListingPage/VetList/vetList';
import shelterList from './Components/ListingPage/ShelterList/shelterList';
import Home from './Components/Home/Home';
import PetEntity from './Components/Entities/PetEntities/PetEntity';
import ShelterEntity from './Components/Entities/ShelterEntities/ShelterEntity';
import VetEntity from './Components/Entities/VetEntities/VetEntity';
// import breedList from './Components/ListingPage/BreedList/breedList';

class App extends Component {
  componentWillMount() {
    document.title = 'Connect Pets To Me';
  }
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Navbar/>
            <Route exact path='/' component={Home}/>
            <Route path='/About' component={About}/>
            <Route path='/FindPets' component={PetList}/>
            <Route path='/FindVets' component={vetList}/>
            <Route path='/FindShelters' component={shelterList}/>
            <Route path='/Pets/PetEntity/:petId' component={PetEntity}/>
            <Route path='/Shelters/ShelterEntity/:shelterId' component={ShelterEntity}/>
            <Route path='/Vets/VetEntity/:vetId' component={VetEntity}/>
            {/*<Route path='/BreedList' component={breedList}/>*/}
            <Footer/>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
