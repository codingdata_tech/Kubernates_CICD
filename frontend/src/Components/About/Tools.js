import React, { Component } from 'react'

export default class Tools extends Component {

  render() {
    return (                  
      <div className="col-md-3 d-flex align-items-stretch">
        <div className="card mb-3 box-shadow">
          <div className="card-body">
            <h5 className="card-title">{this.props.name}</h5>
            <p className="card-text">{this.props.description}</p>
          </div> 
        </div>
      </div>
    )
  }
}