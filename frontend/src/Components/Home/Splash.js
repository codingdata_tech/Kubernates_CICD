import React, {Component} from 'react';
import './Splash.css';

class Splash extends Component {
  render(){
    return(
      <div className = "splash">
        <section className="jumbotron text-center">
          <div className="container">
            <h1 className="jumbotron-heading">{this.props.header}</h1>
            <p className="lead text-dark">{this.props.text}</p>
          </div>
        </section> 
      </div>
      )
  }
}

export default Splash;