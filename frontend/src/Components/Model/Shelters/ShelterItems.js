import React, { Component } from 'react'
import RatingStar from '../../RatingStar/RatingStar';
import { Link } from 'react-router-dom'

export default class shelterItems extends Component {
  render() {
    return (
    <div className="col-md-4">
        <div className="card mb-4 box-shadow">
          <Link to={"/Shelters/ShelterEntity/".concat(this.props.shelterData.id)}><img className="card-img-top" src={this.props.shelterData.image} alt="Card cap"/></Link>
          <div className="card-body">
            <Link to={"/Shelters/ShelterEntity/".concat(this.props.shelterData.id)}><p className="card-text">{this.props.shelterData.name}</p></Link>
            {/*<small className="text-muted">11 miles away</small>*/}
            <RatingStar rating={this.props.shelterData.rating}/>
            <br/>
            <div className="d-flex justify-content-between align-items-center">
              <div className="btn-group">
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.shelterData.location}</button>
                <button type="button" className="btn btn-sm btn-outline-secondary">{this.props.shelterData.contact}</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
