import {FETCH_PET_ENTRY} from '../actions/type';

const initialState = {
    items: [],
    item: {}
};

export default function(state=initialState, action){
    switch(action.type){
        case FETCH_PET_ENTRY:
            return{
                ...state,
                items: action.data
            };
        default:
            return state;
    }
}
